# Deployment Scripts

## Ingress Deployment
Source of Helm Chart - https://github.com/kubernetes/ingress-nginx

Create a namespace for your ingress resources

    kubectl create namespace ingress-basic

Add the ingress-nginx repository

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

Deploy an NGINX ingress controller

    helm install nginx-ingress ingress-nginx/ingress-nginx --namespace ingress-basic --set controller.replicaCount=2  --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux --set controller.admissionWebhooks.patch.nodeSelector."beta\.kubernetes\.io/os"=linux


## PostgreSQL Deployment

TO DEPLOY:
```
helm install postgresql-stock --values pdb-values.yaml --set postgresqlDatabase=stock bitnami/postgresql
helm install postgresql-payment --values pdb-values.yaml --set postgresqlDatabase=payment bitnami/postgresql
helm install postgresql-order --values pdb-values.yaml --set postgresqlDatabase=order bitnami/postgresql
```

TO FULLY DELETE:
```
helm uninstall postgresql-stock
helm uninstall postgresql-payment
helm uninstall postgresql-order
kubectl.exe delete pvc data-postgresql-stock-postgresql-0
kubectl.exe delete pvc data-postgresql-payment-postgresql-0
kubectl.exe delete pvc data-postgresql-order-postgresql-0
```

NOTES:
**Please be patient while the chart is being deployed**

PostgreSQL can be accessed via port 5432 on the following DNS name from within your cluster:

    postgresql.default.svc.cluster.local - Read/Write connection

To get the password for "postgres" run:

    export POSTGRES_PASSWORD=$(kubectl get secret --namespace default postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

To connect to your database run the following command:

    kubectl run postgresql-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/postgresql:11.12.0-debian-10-r20 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgresql -U postgres -d postgres -p 5432



To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace default svc/postgresql 5432:5432 &
    PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U postgres -d postgres -p 5432

## CockroachDB
TO DEPLOY:
```
helm install cockroachdb --values cdb-values.yaml cockroachdb/cockroachdb
```
TO FULLY DELETE:
```
helm uninstall cockroachdb
kubectl.exe delete pvc datadir-cockroachdb-0 datadir-cockroachdb-1 datadir-cockroachdb-2 datadir-cockroachdb-3 datadir-cockroachdb-4
```

NOTES:
CockroachDB can be accessed via port 26257 at the
following DNS name from within your cluster:

`cockroachdb-public.default.svc.cluster.local`

Because CockroachDB supports the PostgreSQL wire protocol, you can connect to
the cluster using any available PostgreSQL client.

For example, you can open up a SQL shell to the cluster by running:

    kubectl run -it --rm cockroach-client \
        --image=cockroachdb/cockroach \
        --restart=Never \
        --command -- \
        ./cockroach sql --insecure --host=cockroachdb-public.default

From there, you can interact with the SQL shell as you would any other SQL
shell, confident that any data you write will be safe and available even if
parts of your cluster fail.

Finally, to open up the CockroachDB admin UI, you can port-forward from your
local machine into one of the instances in the cluster:

    kubectl port-forward cockroachdb-0 8080

Then you can access the admin UI at http://localhost:8080/ in your web browser.

TO DEPLOY:
helm install rabbitmq bitnami/rabbitmq --set persistence.storageClass=default --set replicaCount=3

TO FULLY DELETE:
helm uninstall rabbitmq
kubectl.exe delete pvc data-rabbitmq-0 data-rabbitmq-1 data-rabbitmq-2

## RabbitMQ

NOTES:

`echo "Username: user"`

`echo "Password: $(kubectl get secret --namespace default rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)"`

`echo "ErLang Cookie: $(kubectl get secret --namespace default rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)"`

RabbitMQ can be accessed within the cluster on port at `rabbitmq.default.svc`.

To access for outside the cluster, perform the following steps:

To Access the RabbitMQ AMQP port:

    echo "URL : amqp://127.0.0.1:5672/"
    kubectl port-forward --namespace default svc/rabbitmq 5672:5672

To Access the RabbitMQ Management interface:

    echo "URL : http://127.0.0.1:15672/"
    kubectl port-forward --namespace default svc/rabbitmq 15672:15672

## Horizontal Pod Autoscalers 
Horizontal Pod Autoscalers(HPA) are correctly configured but they are not working due to the fact that service and consumer deployments do not define limit and request, which is required for HPA to automatically scale pods. 

Kubernetes documentation describing limits and requests can be found here: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#resource-units-in-kubernetes

